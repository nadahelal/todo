from rest_framework import generics
from .models import Todo
from .serializers import Todoserializer

# Create your views here.
class ListTodo(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = Todoserializer
class DetailTodo(generics.RetrieveAPIView):
    queryset = Todo.objects.all()
    serializer_class = Todoserializer

